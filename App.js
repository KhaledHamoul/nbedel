import React from 'react';
import { Platform, StackNavigator, StatusBar, StyleSheet, View, AsyncStorage, ToastAndroid } from 'react-native';
import { AppLoading, SplashScreen, Asset, Font, Icon } from 'expo';
import AppNavigator from './navigation/AppNavigator';
import AppHomeScreen from './screens/AppHomeScreen';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
          autoHideSplash={false}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />} 
          <AppNavigator></AppNavigator>
        </View>
      );
    }
  }

  _loadResourcesAsync = async () => {

    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/header.png'),
        require('./assets/images/chart.png'),
        require('./assets/images/bel.png')
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        'Montserrat-Black': require('./assets/fonts/Montserrat-Black.ttf'),
        'Montserrat-Bold': require('./assets/fonts/Montserrat-Bold.ttf'),
        'Montserrat-Light': require('./assets/fonts/Montserrat-Light.ttf'),
        'Montserrat-Medium': require('./assets/fonts/Montserrat-Medium.ttf'),
        'Montserrat-Regular': require('./assets/fonts/Montserrat-Regular.ttf'),

      }),

    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
         
    fetch('http://backend.nbedel.com/api/get-with-all-changes')
    //fetch('http://192.168.43.131:8000/api/get-with-last-changes')
      .then(async (data) => {
        try {
          await AsyncStorage.multiSet([['@storage:data', String(data._bodyText)], ['@storage:dataIsSet', '0']])
          console.log('data stored successfully')
          global.devisesData = JSON.parse(String(data._bodyText))
          //this.setState({ devisesData: JSON.parse(String(data._bodyText)) })
          //this.setState({ devisesData: JSON.parse(JSON.parse(String(data._bodyText))) })          
          this.setState({ isLoadingComplete: true });
          SplashScreen.hide()
          ToastAndroid.showWithGravityAndOffset(
            'Données chargées du serveur',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM, 0 , 120
          );
        } catch (error) {
          // Error saving data
          //await AsyncStorage.clear()
          ToastAndroid.showWithGravityAndOffset(
            'L\'application n\'a pas pu se connecter au serveur ou récuppérer des données de cache, veuillez relancer l\'application et vérifier votre connextion internet ',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM, 0 , 120
          );
          console.log('error while storing data ! === ' + error)
        }
      })
      .catch(async (error) => {
        console.log(error)
        AsyncStorage.getItem('@storage:dataIsSet', (err, res) => {
          if (err) console.log('error, dont return app home page !')
          else if (res == null) ToastAndroid.showWithGravityAndOffset(
            'L\'application n\'a pas pu se connecter au serveur ou récuppérer des données de cache, veuillez relancer l\'application et vérifier votre connextion internet ',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM, 0, 120
          );
          else {
            console.log('DATA IS SET = ' + res)
            AsyncStorage.getItem('@storage:data', (err, data) => {
              if (err) ToastAndroid.showWithGravityAndOffset(
                'L\'application n\'a pas pu se connecter au serveur ou récuppérer des données de cache, veuillez relancer l\'application et vérifier votre connextion internet ',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM, 0, 120
              );
              else {
                ToastAndroid.showWithGravityAndOffset(
                  'L\'application n\'a pas pu se connecter au serveur, les données sont chargées du cache de votre smartphone ',
                  ToastAndroid.LONG,
                  ToastAndroid.BOTTOM, 0, 120
                );
                console.log('DATA = ' + data)
                global.devisesData = JSON.parse(String(data))

                //this.setState({ devisesData: JSON.parse(String(data)) })
                //this.setState({ devisesData: JSON.parse(JSON.parse(String(data))) })
                this.setState({ isLoadingComplete: true });
                SplashScreen.hide()
              }
            })
          }
        })


      })
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
