import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Linking
} from 'react-native';
import {
    AdMobBanner
} from 'expo';

export default class AppInfoScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    state = {
        btnOpacity: 0.6,
        generalFontFamilly: 'Montserrat-Light'
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1, flexDirection: 'column', position: 'absolute', top: 0, left: 0, right: 0, width: '100%', height: '89%' }}>
                    <Image source={require('../assets/images/header.png')}
                        style={styles.headerImg}>
                    </Image>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', postion: 'absolute', backgroundColor: 'transparent', top: 0, paddingTop: '9%', left: 0, width: '100%' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 3, height: 20, paddingLeft: 15 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('AppHomeStack')} activeOpacity={0.9} >
                                <Text style={{ color: 'white', fontFamily: this.state.generalFontFamilly, fontSize: 26 }}>Nb<Text style={{ fontSize: 20, fontWeight: '500' }}>€</Text>del</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 5, paddingLeft: 20, paddingRight: 20, flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={{ color: 'white', textAlign: 'justify', lineHeight: 21, fontFamily: 'Montserrat-Light' }}>Nbedel est une application qui vous permet de connaître en temps réel le taux de change Euro (€) Dinars (DZD) sur le marché noir (parallèle), comme officiel.
                        L’application est mise à jour quotidiennement pour garantir à ses utilisateurs une information fiable. {'\n'}
                            Les cours de l’euro et des différentes devises en algérie sont récoltés chaque jour au matin auprès des cambistes du Square Port Saïd, dans le but de mettre à jour nos bases de données.{'\n'}
                            Nous nous sommes efforcés de vous fournir une application au design épuré et simple dans le but de vous aider à vous organiser dans vos affaires et vos voyages facilement.{'\n'}
                            Une fonctionnalité Alerte vous avertit lorsqu’une devise dépasse un certain seuil que vous auriez fixé à l’avance.
                        </Text>

                    </View>
                    <View style={{ paddingTop: 250, alignItems: 'center' }}>
                        <AdMobBanner
                            bannerSize="smartBannerPortrait"
                            adUnitID="ca-app-pub-7575577717278490/9260138420"
                            onDidFailToReceiveAdWithError={this.bannerError} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end' }}>
                        <Text style={{ fontFamily: 'Montserrat-Regular' }}>CoDZone <Text style={{ fontFamily: 'Montserrat-Light' }} >Version 1.0.0</Text> </Text>
                        <Text style={{ fontSize: 12, paddingTop: 5}} onPress={() => Linking.openURL('http://nbedel.com/privacy') }>politique de confidentialité</Text>
                    </View>
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    headerImg: {
        flex: 1,
        resizeMode: 'stretch',
        position: 'relative',
        width: '105%',
        top: 0,
        left: 0,
        right: 0
    },
    inactiveBtnAchatVente: {
        alignItems: 'center',
        padding: 6,
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 2,
        height: 34
    },
    inactiveBtnAchatVenteText: {
        color: '#fff',
        fontFamily: 'Montserrat-Regular'
    },
});