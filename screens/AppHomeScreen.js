import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Share
} from 'react-native';
import {
    LinearGradient, AdMobBanner
} from 'expo';
import { Ionicons } from '@expo/vector-icons';





export default class AppHomeScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    state = {
        venteAchatToggle: true,
        squareOfficielToggle: true,
        btnOpacity: 0.6,
        devisesData: null,
        selectedDevise: null,
        generalFontFamilly: 'Montserrat-Light'

    }

    componentWillMount() {

        this.setState({ selectedDevise: global.devisesData.devises[0] })
        //this.setState({ devisesData: this.propos.devisesData })
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1, flexDirection: 'column', position: 'absolute', top: 0, left: 0, right: 0, width: '100%', height: '60%' }}>
                    <Image source={require('../assets/images/header.png')}
                        style={styles.headerImg}>
                    </Image>
                </View>
                <View style={{ flex:1, flexDirection: 'column', postion: 'absolute', backgroundColor: 'transparent', top: 0, paddingTop: '9%', left: 0, width: '100%' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1, height: 20, paddingLeft: 15 }}>
                            <Text style={{ color: 'white', fontFamily: this.state.generalFontFamilly, fontSize: 26 }}>Nb<Text style={{ fontSize: 20, fontWeight: '500' }}>€</Text>del</Text>
                        </View>
                        <View style={{ flex: 2, flexDirection: 'row', marginLeft: 50, paddingTop: 5, height: 20 }}>
                            <View style={{ flex: 1, marginHorizontal: 6 }}>
                                <TouchableOpacity activeOpacity={this.state.btnOpacity} onPress={this._achatBtnHandelClicking} style={this.state.venteAchatToggle ? styles.activeBtnAchatVente : styles.inactiveBtnAchatVente}>
                                    <Text style={this.state.venteAchatToggle ? styles.activeBtnAchatVenteText : styles.inactiveBtnAchatVenteText}> Vente </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, marginHorizontal: 6, marginRight: 10 }}>
                                <TouchableOpacity activeOpacity={this.state.btnOpacity} onPress={this._venteBtnHandelClicking} style={this.state.venteAchatToggle ? styles.inactiveBtnAchatVente : styles.activeBtnAchatVente}>
                                    <Text style={this.state.venteAchatToggle ? styles.inactiveBtnAchatVenteText : styles.activeBtnAchatVenteText}> Achat </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 3.6, flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={{ flex: 1.7, color: 'white', fontFamily: this.state.generalFontFamilly, fontSize: 40 }}>1{this.state.selectedDevise.symbole} = {this.getApropiatePrice(this.state.selectedDevise.changes[0], 0)}.<Text style={{ fontSize: 35 }}>{this.getApropiatePrice(this.state.selectedDevise.changes[0], 1)}</Text> <Text style={{ fontSize: 23, fontWeight: '900' }}>DZD</Text></Text>
                        <Text style={{ flex: 1, color: 'white', fontFamily: this.state.generalFontFamilly, fontSize: 10 }}> DERNIERE MISE A JOUR   : <Text>{this.dataFormatHelper(this.state.selectedDevise.changes[0].created_at)}</Text></Text>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', width: '40%' }}>
                            <TouchableOpacity onPress={this.onShare} activeOpacity={this.state.btnOpacity} style={{ backgroundColor: 'white', padding: 12, borderWidth: 1, borderColor: 'white', borderRadius: 100, marginHorizontal: 8, width: 50, height: 50 }}>
                                <Ionicons name="md-share" size={25} color="#b200ba" />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('AppInfoStack')} activeOpacity={this.state.btnOpacity} style={{ backgroundColor: 'white', paddingVertical: 9.5, paddingHorizontal: 11.4, borderWidth: 1, borderColor: 'white', borderRadius: 100, marginHorizontal: 8, width: 50, height: 50 }}>
                                <Ionicons name="md-information-circle-outline" size={30} color="#b200ba" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 2.4, flexDirection: 'row', justifyContent: 'center', width: '80%', marginTop: 50 }}>
                            <TouchableOpacity activeOpacity={this.state.btnOpacity} onPress={this._squareBtnHandelClicking} style={this.state.squareOfficielToggle ? styles.activeBtnSquareOfficiel : styles.inactiveBtnSquareOfficiel}>
                                <Text style={this.state.squareOfficielToggle ? { color: '#b200ba', fontFamily: 'Montserrat-Medium', fontSize: 10 } : { color: 'white', fontFamily: 'Montserrat-Medium', fontSize: 10 }}>Taux Marché Noir</Text>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={this.state.btnOpacity} onPress={this._officielBtnHandelClicking} style={this.state.squareOfficielToggle ? styles.inactiveBtnSquareOfficiel : styles.activeBtnSquareOfficiel}>
                                <Text style={this.state.squareOfficielToggle ? { color: 'white', fontFamily: 'Montserrat-Medium', fontSize: 10 } : { color: '#b200ba', fontFamily: 'Montserrat-Medium', fontSize: 10 }}>Taux Officiel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ flex: 2.7, flexDirection: 'column', alignItems: 'center', paddingTop: 30 }}>
                        <Text style={{ flex: 0.2, color: '#691da9', fontFamily: 'Montserrat-Bold', fontSize: 13 }}>Le Taux De Change Pour D'autres Devises</Text>
                        <ScrollView style={{ flex: 2, width: '100%' }}>

                            {
                                this.renderDeviseElements()
                            }


                        </ScrollView>
                    </View>
                    <View style={{ alignItems: 'center' , height: 21 }}>
                        <AdMobBanner
                            bannerSize="smartBannerPortrait"
                            adUnitID="ca-app-pub-7575577717278490/9260138420" 
                            onDidFailToReceiveAdWithError={this.bannerError} />
                    </View>
                </View>
            </View>

        );
    }

    bannerError = (e) => {
        console.log('error displaying banner !' + e )
    }

    _achatBtnHandelClicking = async () => {
        console.log('Achat !');
        this.setState({ venteAchatToggle: true });
    }

    _venteBtnHandelClicking = async () => {
        console.log('Vente !');
        this.setState({ venteAchatToggle: false })
    }

    _squareBtnHandelClicking = async () => {
        console.log('Square !');
        this.setState({ squareOfficielToggle: true });
    }

    _officielBtnHandelClicking = async () => {
        console.log('Officiel !');
        this.setState({ squareOfficielToggle: false })
    }

    dataFormatHelper = (date) => {
        var d = new Date(String(date).slice(0, 10))
        return String('  ' + d.getDate() + ' / ' + (d.getMonth()+1) + ' / ' + d.getFullYear())
    }


    renderDeviseElements = () => {
        return global.devisesData.devises.map((item) => {
            return (
                <TouchableOpacity key={item.id} activeOpacity={0.5} onPress={() => this._selectItem(item)} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', marginBottom: 10, padding: 8, backgroundColor: '#e9e9e9', height: 70, borderRadius: 20, marginHorizontal: 15 }}>
                    <LinearGradient colors={[item.couleur_gauche, item.couleur_droite]} start={[0, 0.5]} end={[1, 0.5]} style={{ flex: 1.1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'green', height: 50, width: 50, borderRadius: 15, marginLeft: 3, marginRight: 10, }}>
                        <Text style={{ fontSize: 26, color: '#fff', fontFamily: 'Montserrat-Medium' }}>{item.symbole}</Text>
                    </LinearGradient>
                    <View style={{ flex: 3, paddingTop: 10 }}>
                        <Text style={{ flex: 1, fontFamily: 'Montserrat-Bold' }}>{item.nom}</Text>
                        <Text style={{ flex: 1.4, fontFamily: 'Montserrat-Regular', fontSize: 10, color: '#a8a8a8' }}> {item.code}</Text>
                    </View>
                    <View style={{ flex: 2.7, paddingTop: 5, alignItems: 'center' }}>
                        <Text style={{ flex: 1.2, fontSize: 20, fontFamily: 'Montserrat-Medium' }}>{this.getApropiatePrice(item.changes[0], 0)}.<Text style={{ fontSize: 17 }}>{this.getApropiatePrice(item.changes[0], 1)} DZD</Text></Text>
                        <Text style={{ flex: 1, color: '#691da9', fontFamily: 'Montserrat-Medium', fontSize: 12 }}> {this.state.venteAchatToggle ? 'PRIX DE VENTE' : 'PRIX D\'ACHAT'} </Text>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    _selectItem = (item) => {
        console.log(item)
        this.setState({ selectedDevise: item })
    }

    getApropiatePrice = (item, part) => {
        if (this.state.squareOfficielToggle)
            if (this.state.venteAchatToggle) return String(item.vente_square).split('.')[part];
            else return String(item.achat_square).split('.')[part];
        else
            if (this.state.venteAchatToggle) return String(item.vente_officiel).split('.')[part];
            else return String(item.achat_officiel).split('.')[part];
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message: global.devisesData.share_link,
                title: global.devisesData.share_title
            }, {
                    dialogTitle: global.devisesData.share_dialog_title
                })

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('shared with activity.resultTab')
                } else {
                    // shared
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
                console.log('dismissed')
            }
        } catch (error) {
            alert(error.message);
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    headerImg: {
        flex: 1,
        resizeMode: 'stretch',
        position: 'relative',
        width: '100%',
        top: 0,
        left: 0,
        right: 0
    },
    activeBtnAchatVente: {
        alignItems: 'center',
        padding: 8,
        backgroundColor: '#fff',
        borderRadius: 2,
        height: 34
    },
    activeBtnAchatVenteText: {
        color: '#b200ba',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12
    },
    inactiveBtnAchatVente: {
        alignItems: 'center',
        padding: 8,
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 2,
        height: 34
    },
    inactiveBtnAchatVenteText: {
        color: '#fff',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12
    },
    activeBtnSquareOfficiel: {
        flex: 1,
        backgroundColor: '#fff',
        height: 36,
        padding: 9,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 100,
        marginHorizontal: 8
    },
    inactiveBtnSquareOfficiel: {
        flex: 1,
        height: 36,
        padding: 9,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 100,
        marginHorizontal: 8
    }
});