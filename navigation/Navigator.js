import { createStackNavigator , createSwitchNavigator } from 'react-navigation';


import AppHomeScreen from '../screens/AppHomeScreen';
import AppInfoScreen from '../screens/AppInfoSreen';


const AppHomeStack = createStackNavigator({
    AppInfo:{
        screen: AppHomeScreen
    }
});

const AppInfoStack = createStackNavigator({
    AppInfo:{
        screen: AppInfoScreen
    }
});

export default createStackNavigator({
    AppHomeStack,
    AppInfoStack
},{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
});

